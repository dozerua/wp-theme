<?php

function bst_widgets_init()
{

    /*
    Sidebar (one widget area)
     */
    register_sidebar(array(
        'name' => __('Sidebar', 'bst'),
        'id' => 'sidebar-widget-area',
        'description' => __('The sidebar widget area', 'bst'),
        'before_widget' => '<section class="%1$s %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ));

    /*
    Footer (three widget areas)
     */
    register_sidebar(array(
        'name' => __('Footer', 'bst'),
        'id' => 'footer-widget-area',
        'description' => __('The footer widget area', 'bst'),
        'before_widget' => '<div class="%1$s %2$s col-sm-2">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ));

    /*
   Footer Copyright (three widget areas)
    */
    register_sidebar(array(
        'name' => __('Footer Copyright', 'bst'),
        'id' => 'copyright-widget-area',
        'description' => __('The footer copyright widget area', 'bst'),
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ));

    /*
    Home page bottom slider
     */
    register_sidebar(array(
        'name' => __('Homepage bottom slider', 'bst'),
        'id' => 'homepagebottomslider-widget-area',
        'description' => __('The homepage bottom slider widget area', 'bst'),
        'before_widget' => '<div class="container-fluid homepage-bottom-slider">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ));

    /*
    Home page right column
     */
    register_sidebar(array(
        'name' => __('Homepage right column', 'bst'),
        'id' => 'homepagerightcolumn-widget-area',
        'description' => __('The homepage right column widget area', 'bst'),
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ));

    /*
   Petition
    */
    register_sidebar(array(
        'name' => __('Petition', 'bst'),
        'id' => 'petition-widget-area',
        'description' => __('The petition widget area', 'bst'),
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ));


}

add_action('widgets_init', 'bst_widgets_init');
