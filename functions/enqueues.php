<?php

function bst_enqueues()
{

    wp_register_script('jquery', get_bloginfo('template_url') . '/js/jquery-1.11.3.min.js', __FILE__, false, '1.11.3', true);
    wp_enqueue_script('jquery');

    wp_register_style('bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', false, '3.3.4', null);
    wp_enqueue_style('bootstrap-css');

    wp_register_style('font_awesome-css', get_template_directory_uri() . '/css/font-awesome.css', false, null);
    wp_enqueue_style('font_awesome-css');

    wp_register_style('bst-css', get_template_directory_uri() . '/css/bst.css', false, null);
    wp_enqueue_style('bst-css');

    wp_register_style('bst-common-css', get_template_directory_uri() . '/css/bst-common.css', false, null);
    wp_enqueue_style('bst-common-css');

    wp_register_style('bst-footer-css', get_template_directory_uri() . '/css/bst-footer.css', false, null);
    wp_enqueue_style('bst-footer-css');

    wp_register_style('bst-menu-css', get_template_directory_uri() . '/css/bst-menu.css', false, null);
    wp_enqueue_style('bst-menu-css');

    wp_register_script('modernizr', get_template_directory_uri() . '/js/modernizr-2.8.3.min.js', false, null, true);
    wp_enqueue_script('modernizr');

    wp_register_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', false, null, true);
    wp_enqueue_script('bootstrap-js');

    wp_register_script('bst-js', get_template_directory_uri() . '/js/bst.js', false, null, true);
    wp_enqueue_script('bst-js');

    wp_register_script('bst-menu-js', get_template_directory_uri() . '/js/bst-menu.js', false, null, true);
    wp_enqueue_script('bst-menu-js');

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'bst_enqueues', 100);
