<?php
/*
The Single Posts Loop
=====================
*/
?> 

<?php if(have_posts()): while(have_posts()): the_post(); ?>
    <article role="article" id="post_<?php the_ID()?>" <?php post_class()?>>
        <section>
			<?php $post_id = get_the_ID(); ?>
            <?php the_content()?>
            <?php wp_link_pages(); ?>
        </section>
    </article>
<?php //comments_template('/includes/loops/comments.php'); ?>
<?php endwhile; ?>
<?php else: ?>
<?php wp_redirect(get_bloginfo('siteurl').'/404', 404); exit; ?>
<?php endif; ?>
<?php $cat = get_the_category($post->ID);
$cat_id=$cat[0]->cat_ID; ?>
<?php if ($cat_id==3){ ?>
<div class="row next_petition">	
<h3>Steun ook onze andere petities:</h3>
<?php
$exclude_ids = array( $post_id );
$query = new WP_Query( array( 'posts_per_page' => 6,'cat' => 3, 'post__not_in' => $exclude_ids  ) );
$i=1;
if($query->have_posts()): while($query->have_posts()): $query->the_post();?>
<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
<div class="next_petition_block" id="pet<?php echo $i; ?>">
<?php the_post_thumbnail(); ?>
<?php  if (get_field('id_petition')>0)
$shortcode='[emailpetition id="'.get_field('id_petition').'"]';
echo do_shortcode($shortcode);
global $exp;
if ($exp==0) {
	?>
<h3><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h3>
<?php the_excerpt();  ?>
<p class="teken_petite"><a class="btn btn-white" href="<?php the_permalink(); ?>">Teken Petitie</a></p>
<?php } ?>
</div>
</div>
<?php 
$i=$i+1;
endwhile;
endif;
?>	
</div>	
<?php }
?>