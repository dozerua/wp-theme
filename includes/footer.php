<footer class="site-footer">
    <div class="container">
        <div class="row icons">
            <div class="col-sm-4 logo">
                <img src="<?php echo get_bloginfo('template_url') ?>/img/footer/logo.png"/>
            </div>
            <div class="col-sm-4 text-center social">
                <img src="<?php echo get_bloginfo('template_url') ?>/img/footer/facebook.png"/>
                <img src="<?php echo get_bloginfo('template_url') ?>/img/footer/twitter.png"/>
            </div>
            <div class="col-sm-4 text-right">
                <img src="<?php echo get_bloginfo('template_url') ?>/img/footer/trustwave.png"/>
                <img src="<?php echo get_bloginfo('template_url') ?>/img/footer/anbi.png"/>
                <img src="<?php echo get_bloginfo('template_url') ?>/img/footer/giro.png"/>
                <img src="<?php echo get_bloginfo('template_url') ?>/img/footer/cbf.png"/>
            </div>
        </div>
        <div class="row links">
            <?php dynamic_sidebar('footer-widget-area'); ?>
            <div class="col-sm-4 text-right">
                <div class="row">
                    <button class="btn btn-red contact">CONTACT</button>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <?php dynamic_sidebar('copyright-widget-area') ?>
            </div>
            <div class="col-sm-4">
                <div class="dropup language">
                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                        Change Language...
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="#">English</a></li>
                        <li><a href="#">Russian</a></li>
                        <li><a href="#">Chinese</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<?php wp_footer(); ?>
</body>
</html>
