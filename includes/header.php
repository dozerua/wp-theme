<!DOCTYPE html>
<html class="no-js">
<head>
    <title><?php wp_title('•', true, 'right');
        bloginfo('name'); ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!--[if lt IE 8]>
<div class="alert alert-warning">
    You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your
    browser</a> to improve your experience.
</div>
<![endif]-->

<div class="container-fluid">
    <?php putRevSlider('homepageslider', 'homepage'); ?>
</div>

<div id="main-menu-wrapper">
    <nav id="main-menu" class="navbar navbar-default navbar-static-top">
        <div class="container">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a id="site-title"
                   href="<?php echo home_url('/'); ?>"
                   title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"
                   rel="home">
                    <img class="logo" src="<?php echo get_bloginfo('template_url') ?>/img/logo.png">
                </a>
            </div>

            <div class="collapse navbar-collapse" id="navbar">
                <?php
                wp_nav_menu(array(
                        'theme_location' => 'navbar-left',
                        'depth' => 2,
                        'menu_class' => 'nav navbar-nav',
                        'fallback_cb' => 'wp_bootstrap_navwalker::fallback')
                );
                ?>

            </div><!-- /.navbar-collapse -->
            <?php if (is_front_page()): ?>
                <div class="donate">
                    <a class="btn btn-red" href="#">DONATE</a>
                </div>
            <?php endif; ?>
        </div><!-- /.container -->
    </nav>
</div>

<!--
Site Title
==========
If you are displaying your site title in the "brand" link in the Bootstrap navbar, 
then you probably don't require a site title. Alternatively you can use the example below. 
See also the accompanying CSS example in css/bst.css .

<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <h1 id="site-title">
      	<a class="text-muted" href="<?php echo home_url('/'); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php bloginfo('name'); ?></a>
      </h1>
    </div>
  </div>
</div>
-->
