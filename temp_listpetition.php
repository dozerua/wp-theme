<?php //Template Name:List Petition
get_template_part('includes/header'); ?>

<div class="container">
  <div class="row">

    <div class="col-xs-12 col-sm-8">
      <div id="content" role="main">
      <?php  $query = new WP_Query( array( 'posts_per_page' => 8,'cat' => 3  ) );
if($query->have_posts()): while($query->have_posts()): $query->the_post();?>
<div class="one_petition">
<div class="col-xs-12 col-sm-3">
<?php the_post_thumbnail(); ?>
</div>
<div class="col-xs-12 col-sm-9">
<h3><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h3>
<p><span class="text-muted author_petition"><?php _e('Von', 'bst'); echo " "; the_author() ?>,</span>
<?php  if (get_field('id_petition')>0)
$shortcode='[emailpetition id="'.get_field('id_petition').'"]';
//echo do_shortcode($shortcode);
	?>
</p>
</div>
</div>
<?php 
endwhile;
endif;
?>	
      </div><!-- /#content -->
    </div>
    
   <div class="col-xs-12 col-sm-4" id="sidebar" role="navigation">
      <?php get_template_part('includes/sidebar'); ?>
    </div>
    
  </div><!-- /.row -->
</div><!-- /.container -->

<?php get_template_part('includes/footer'); ?>
