<?php get_template_part('includes/header'); ?>

<div class="container">
    <div class="row">
        <div class="col-sm-8">
            <div id="content" role="main">
                <?php if (have_posts()): while (have_posts()): the_post(); ?>
                    <article role="article" id="post_<?php the_ID() ?>" <?php post_class() ?>>
                        <?php the_content() ?>
                        <?php wp_link_pages(); ?>
                    </article>
                <?php endwhile; else: ?>
                    <?php wp_redirect(get_bloginfo('siteurl') . '/404', 404); ?>
                    <?php exit; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-sm-4" id="">
            <div class="sidebar">
                <?php
                $query = new WP_Query(array('posts_per_page' => 1, 'cat' => 26));
                if ($query->have_posts()):
                    while ($query->have_posts()):
                        $query->the_post();
                        ?>
                        <h3>Help ons een leven te redden.</h3>
                        <div class="next_petition_block">
                            <?php the_post_thumbnail(); ?>
                            <?php if (get_field('id_petition') > 0)
                                $shortcode = '[emailpetition id="' . get_field('id_petition') . '"]';
                            echo do_shortcode($shortcode);
                            global $exp;
                            if ($exp == 0) :
                                ?>
                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                <?php the_excerpt(); ?>
                                <p class="teken_petite"><a class="btn btn-white" href="<?php the_permalink(); ?>">Teken
                                        Petitie</a></p>
                            <?php endif; ?>
                        </div>
                    <?php
                    endwhile;
                endif;
                dynamic_sidebar('homepagerightcolumn-widget-area');
                ?>
            </div>
        </div>
    </div>
</div><!-- /.container -->
<?php
$query = new WP_Query(array('posts_per_page' => 3, 'cat' => 28));
if ($query->have_posts()):
    ?>
    <div class="container-fluid home-miniature-block indent-top">
        <div class="container">
            <?php
            while ($query->have_posts()):
                $query->the_post();
                ?>
                <div class="col-sm-4">
                    <div class="home-post-miniature">
                        <div class="image">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_post_thumbnail(); ?>
                            </a>
                        </div>
                        <div class="info">
                            <div class="clearfix">
                                <a class="title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </div>
                            <div class="clearfix">
                                <div class="date">
                                    <small><?php
                                        $previousday = null;
                                        the_date();
                                        ?></small>
                                </div>
                                <div class="arrow">
                                    <a href="<?php the_permalink(); ?>">
                                        <img src="<?php echo get_bloginfo('template_url') ?>/img/home/miniature-arrow.png"/>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            endwhile;
            ?>
        </div>
    </div>
<?php
endif;
?>

<?php dynamic_sidebar('homepagebottomslider-widget-area'); ?>

<?php get_template_part('includes/footer'); ?>
