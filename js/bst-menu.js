(function ($) {

    "use strict";

    $(document).ready(function () {
        var mainMenuWrapper,
            mainMenu,
            offset,
            processing;

        mainMenu = $('#main-menu');
        mainMenuWrapper = $('#main-menu-wrapper');
        processing = false;

        mainMenu.find('li').hover(
            function () {
                $(this).find('ul').show();
            },
            function () {
                $(this).find('ul').hide();
            }
        );

        function resetOffset() {
            offset = mainMenuWrapper.offset().top;
        }

        function checkMenu() {
            processing = true;

            if ($(document).scrollTop() > offset) {
                mainMenu.removeClass('navbar-static-top');
                mainMenu.addClass('navbar-fixed-top');
            } else {
                mainMenu.addClass('navbar-static-top');
                mainMenu.removeClass('navbar-fixed-top');
            }

            mainMenuWrapper.height(mainMenu.outerHeight());

            processing = false;
        }

        $(document).on('scroll', function () {
            if (!processing) {
                checkMenu();
            }
        });

        $(window).on('resize', function () {
            if (!processing) {
                resetOffset();
                checkMenu();
            }
        });

        resetOffset();
        mainMenuWrapper.height(mainMenu.height());
    });

}(jQuery));
