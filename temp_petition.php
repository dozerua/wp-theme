<?php //WP Post Template: Petite Template
get_template_part('includes/header'); ?>
<div class="petition_title"><h1><?php the_title() ?></h1></div>
<div class="container">
    <div class="row">

        <div class="col-xs-12 col-sm-12">
            <div id="content" role="main">
                <?php if (have_posts()): while (have_posts()): the_post(); ?>
                    <article role="article" id="post_<?php the_ID() ?>" <?php post_class() ?>>
                        <section>
                            <?php $post_id = get_the_ID(); ?>
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                <?php the_content() ?>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <h3>Onderteken de petitie</h3>

                                <?php $shortcode = '[emailpetition id="' . get_field('id_petition') . '" mail_chimp_id=' . get_field('mailchimp_list_id') . ']';
                                echo do_shortcode($shortcode);
                                dynamic_sidebar('petition-widget-area');
                                ?>
                            </div>

                        </section>
                    </article>
                <?php endwhile; ?>
                <?php else: ?>
                    <?php wp_redirect(get_bloginfo('siteurl') . '/404', 404);
                    exit; ?>
                <?php endif; ?>

                <div class="row next_petition">
                    <h3>Steun ook onze andere petities:</h3>
                    <?php
                    $exclude_ids = array($post_id);
                    $query = new WP_Query(array('posts_per_page' => 6, 'cat' => 3, 'post__not_in' => $exclude_ids));
                    $i = 1;
                    if ($query->have_posts()): while ($query->have_posts()): $query->the_post(); ?>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                            <div class="next_petition_block" id="pet<?php echo $i; ?>">
                                <?php the_post_thumbnail(); ?>
                                <?php if (get_field('id_petition') > 0)
                                    $shortcode = '[emailpetition id="' . get_field('id_petition') . '"]';
                                echo do_shortcode($shortcode);
                                global $exp;
                                if ($exp == 0) {
                                    ?>
                                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                    <?php the_excerpt(); ?>
                                    <p class="teken_petite"><a class="btn btn-white" href="<?php the_permalink(); ?>">Teken Petitie</a></p>
                                <?php } ?>
                            </div>
                        </div>
                        <?php
                        $i = $i + 1;
                    endwhile;
                    endif;
                    ?>
                </div>

            </div><!-- /#content -->
        </div>


    </div><!-- /.row -->
</div><!-- /.container -->

<?php get_template_part('includes/footer'); ?>
